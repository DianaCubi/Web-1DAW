FROM httpd:2.4
COPY ./index.html /usr/local/apache2/htdocs/
COPY ./css /usr/local/apache2/htdocs/css
COPY ./images /usr/local/apache2/htdocs/images
EXPOSE 80